###############################################################################
# BERT NER to detect companies and roles in resumes
###############################################################################

import re
import numpy as np
from time import time
import _pickle as pickle
from flask import Flask, request, jsonify

import torch
from pytorch_pretrained_bert import BertTokenizer
from pytorch_pretrained_bert import BertForTokenClassification

from utils.utils import _print, _wrn_print

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
from keras.preprocessing.sequence import pad_sequences


# BERT settings
BERT_META_DIR = 'bert_meta'
BERT_MAX_LEN = 150
MAX_CHAR_LEN = 30
MODEL = 'BERT'


def _load_ent(meta_dir, ent):
    """loads BERT metadata needed for prediction"""
    with open(f'{meta_dir}/{ent}', 'rb') as fl:
        _tmp = pickle.load(fl)
    return _tmp


# Model meta data
_print('loading BERT metadata for predictions ...')
tag2idx = _load_ent(BERT_META_DIR, 'tag2idx.pkl')
tags_vals = _load_ent(BERT_META_DIR, 'tags_vals.pkl')
model = BertForTokenClassification.\
    from_pretrained('bert-base-uncased', num_labels=len(tag2idx))
model.load_state_dict(torch.load(f'{BERT_META_DIR}/bert_ner.pt',
                                 map_location=torch.device('cpu')))
bert_tokenizer = BertTokenizer.\
    from_pretrained('bert-base-uncased', do_lower_case=True)
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
_print('loaded!')


app = Flask(__name__)


@app.route('/')
def welcome():
    print('BERT based company-role extraction!')
    return 'OK'


@app.route('/get-probs-from-txt/', methods=['POST'])
def get_employers():
    """returns company and role probabilities from text"""
    start_time = time()
    orig_txts = request.json.get('txts')
    map_to_orig = int(request.json.get('map_to_orig'))
    txts = [clean_txt(txt) for txt in orig_txts]
    tokenized_txts = \
        [bert_tokenizer.tokenize(txt) for txt in txts]
    txt_probs = _get_tags_from_pred(tokenized_txts)
    txt_probs = _fix_txt_tags(txt_probs)
    if map_to_orig:
        orig_mapped_txt_probs = []
        for txt, tp in zip(orig_txts, txt_probs):
            orig_mapped_txt_probs.append(_revelio_to_orig_txt_map(txt, tp))

    _print(f'_time_ : {round(time() - start_time, 3)}s')

    if map_to_orig:
        return jsonify(orig_mapped_txt_probs)
    else:
        return jsonify(txt_probs)


def clean_txt(txt):
    """pre-processing for BERT"""
    txts = [' '.join(re.findall(r'\w+', txt)) for txt in txt.split(' -NL- ')]
    return ' -nl- '.join(txts)


def _get_tags_from_pred(tokenized_txts):
    """predictions and tagging"""
    input_ids = pad_sequences([bert_tokenizer.convert_tokens_to_ids(txt)
                               for txt in tokenized_txts],
                              maxlen=BERT_MAX_LEN, dtype='long',
                              truncating='post', padding='post')
    input_mask = [[float(i > 0) for i in ii] for ii in input_ids]
    input_ids = torch.tensor(input_ids).to(device)
    input_mask = torch.tensor(input_mask).to(device)
    probs = []
    with torch.no_grad():
        logits = model(input_ids, token_type_ids=None,
                       attention_mask=input_mask)
        logits = logits.detach().cpu().numpy()
        n = len(logits)
        for i in range(n):
            probs.append(np.array(torch.nn.functional.
                         softmax(torch.tensor(logits[i]), dim=1)))
        prob_dict = [[dict(zip(tags_vals, [str(_p) for _p in _probs]))
                      for _probs in p] for p in probs]
        txt_probs = [list(zip(txt, pd)) for txt, pd in 
                     zip(tokenized_txts, prob_dict)]
    return txt_probs


def _fix_txt_tags(txt_probs):
    """bert tokenizer breaks down words, this fucntion fixes that"""
    new_txt_probs = []
    for txt_prob in txt_probs:
        new_probs = []
        for txt, p in txt_prob:
            if txt.startswith('##'):
                continue
            if txt == '-':
                continue
            else:
                new_probs.append(p)
        new_txt = ' '.join([t[0] for t in txt_prob])
        new_txt = \
            new_txt.replace(' ##', '').replace('- nl -', '-nl-').split(' ')
        new_txt_probs.append(list(zip(new_txt, new_probs)))

    return new_txt_probs


def _revelio_to_orig_txt_map(orig_txt, out):
    """revelio text to original text probabilities mapping"""
    revelio_txt_ix = 0
    revelio_to_orig_map = []
    n = len(out)
    for _txt in orig_txt.split(' '):
        _txt = _txt.lower()
        if _txt == '-nl-':
            revelio_to_orig_map.append((_txt, [[_txt, {'COMPANY': 0, 'O': 1,
                                                       'ROLE': 0}]]))
        elif _txt.isalnum():
            while out[revelio_txt_ix][0] != _txt:
                if revelio_txt_ix > n - 2:
                    break
                revelio_txt_ix += 1
            revelio_to_orig_map.append((_txt, [out[revelio_txt_ix]]))
        else:
            alnum = re.findall(r'[0-9a-z]+', _txt)
            if not alnum:
                revelio_to_orig_map.append((_txt,
                                            [[_txt, {'COMPANY': 0, 'O': 1,
                                                     'ROLE': 0}]]))
            else:
                temp = []
                for __txt in alnum:
                    while out[revelio_txt_ix][0] != __txt:
                        if revelio_txt_ix > n - 2:
                            break
                        revelio_txt_ix += 1
                    temp.append(out[revelio_txt_ix])
                revelio_to_orig_map.append((_txt, temp))

    final_map = []
    for ot, rm in zip(orig_txt.split(' '), revelio_to_orig_map):
        if len(rm[1]) > 1:
            final_map.append((ot, _revelio_out_dict_avg(rm[1])))
        else:
            final_map.append((ot, rm[1][0][1]))

    return final_map


def _revelio_out_dict_avg(_dict):
    """squeezing multi revelio maps"""
    return dict(zip(tags_vals,
                    [str(s) for s in
                     np.mean(np.array([[float(a) for a in list(_d[1].values())]
                                       for _d in _dict]), axis=0)]))
