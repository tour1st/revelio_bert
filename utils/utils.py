import time
import json
import csv
import ast
import _pickle as pickle
from datetime import datetime
from termcolor import colored


def timeit(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        if 'log_time' in kw:
            name = kw.get('log_name', method.__name__.upper())
            kw['log_time'][name] = int((te - ts) * 1000)
        else:
            pass
            print('[{}] {}ms'
                  .format(method.__name__, (te - ts) * 1000))
        return result
    return timed

def _print(txt):
    print(colored(f'[I {datetime.now().strftime("%H:%M:%S")}]', 'green') + ' ' + txt)


def _err_print(txt):
    print(colored(f'[E {datetime.now().strftime("%H:%M:%S")}]', 'red') + ' ' + txt)


def _wrn_print(txt):
    print(colored(f'[W {datetime.now().strftime("%H:%M:%S")}]', 'yellow') + ' ' + txt)


def load_pickle_dict(pkl_file):
    with open(pkl_file, 'rb') as fl:
        pkl_dict = pickle.load(fl)
    return pkl_dict


def write_pickle_dict(pkl_file, out_fl):
    with open(pkl_file, 'wb') as fl:
        pickle.dump(out_fl, fl)
