import requests


class CompRoleTenure:
    def __init__(self):
        self.prob = 'get-probs-from-txt'
        self.host = 'http://localhost:5000'

    def get_probs_from_txt(self, txts, map_to_orig=0):
        res = requests.post(
            url=f"{self.host}/{self.prob}/",
            json={"txts": txts, "map_to_orig": map_to_orig}
        )
        return res.json()
